import React, { Component } from 'react';
import posed from 'react-pose';
import './App.css';
import { Query } from 'react-apollo';

import RecipeItem from './Recipe/RecipeItem';

import { GET_ALL_RECIPES } from '../graphql/queries/index';


// react pose component setup
const RecipeList = posed.ul({
  shown: {
    x: '0%',
    transition: { duration: 300 },
    staggerChildren: 100
  },
  hidden: {
    x: '-100%',
  }
});


class App extends Component {

  state = {
    on: false
  }

  componentDidMount(){
    setTimeout(this.slideIn, 200);
  }

  slideIn = () => {
    this.setState({
      on: !this.state.on
    })
  }


  render() {

    const { on } = this.state;

    return (
      <div className="App">
        <h1 className="main-title">Find Recipes You <strong>Love</strong> </h1>
          <Query query={GET_ALL_RECIPES}>
            {({ data, loading, error }) => {
              if(loading) return <div>loading</div>
              if(error){
                console.log(error);
                return <div>error</div>
              }
              //console.log(data)
              return (
                <RecipeList pose={on ? "shown" : "hidden"} className="cards">
                {data.getAllRecipes.map(recipe => {
                  return <RecipeItem key={recipe._id} {...recipe}/>
                })}
                </RecipeList>
              )
            }}
          </Query>
      </div>
    );
  }
}

export default App;
