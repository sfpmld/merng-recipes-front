import React, { Component } from 'react'; import { Mutation } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import Error from '../Error';
import { SIGNIN_USER } from '../../graphql/mutations/index';

const initialState = {
  email: "",
  password: "",
}

class Signin extends Component {

  // initialize state
  state = {
    ...initialState
  }


  // reinit state function
  clearState = () => {
    this.setState({
      ...initialState
    })
  }

  // set state on the fly
  onChangeHandler = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value
    });
  }

  // validate form before submission
  validateForm = () => {
    const { email, password } = this.state;

    const isInvalid = !email || !password;

    // if invalid input return true
    return isInvalid;

  }

  onSubmitHandler = async (event, signinUser) => {
    //to prevent page reload
    event.preventDefault();

    // retrieving user datas
    const { data } = await signinUser();

    // save user token datas in localStorage
    localStorage.setItem('token', data.signinUser.token);

    // refresh mutation to get fresh datas
    await this.props.refetch();

    //cleanig input when form submitted
    this.clearState();

    ////console.log(data);

    //redirect to home when succeed
    this.props.history.push('/');

  }


  render() {
    const { email, password } = this.state
    return (

      <div className="App">
        <h2 className="App">Signin</h2>
          <Mutation mutation={SIGNIN_USER} variables={{data: {
            email,
            password
          }}}>
            {(signinUser, { loading, error }) => {
              if(loading) return <div>loading</div>
              return (
                <div>
                  <form
                    className="form"
                    onSubmit={event => this.onSubmitHandler(event, signinUser)}
                  >
                    <input
                      type="text"
                      name="email"
                      onChange={this.onChangeHandler}
                      value={this.state.email}
                      placeholder="Email Address"
                    />
                    <input
                      type="password"
                      name="password"
                      onChange={this.onChangeHandler}
                      value={this.state.password}
                      placeholder="Password"
                    />
                    <button className="button-primary" type="submit" disabled={loading || this.validateForm()}>
                      Submit
                    </button>
                  </form>
                  {error && <Error error={error}/>}
                </div>
              );
            }}
          </Mutation>
      </div>
    )
  }
}


export default withRouter(Signin);

