import React from 'react';
import { withRouter } from 'react-router-dom'
import { ApolloConsumer } from 'react-apollo';

const handleSignout = (client, history) => {
  // clearing the token
  localStorage.setItem('token', '');

  // removing session
  client.resetStore();
  console.log(history);

  // redirecting to home page
  history.push('/');
}

const Signout = ({ history }) => (

  <ApolloConsumer>
    {(client) => {

      return (
        <button onClick={() => handleSignout(client, history)}>Signout</button>
      )
    }}
  </ApolloConsumer>

)


export default withRouter(Signout);
