import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { withRouter } from 'react-router-dom';

import Error from '../Error';
import { SIGNUP_USER } from '../../graphql/mutations/index';

const initialState = {
  username: "",
  email: "",
  password: "",
  passwordConfirmation: ""
}

class Signup extends Component {

  // initialize state
  state = {
    ...initialState
  }


  // reinit state function
  clearState = () => {
    this.setState({
      ...initialState
    })
  }

  // set state on the fly
  onChangeHandler = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value
    });
  }

  // validate form before submission
  validateForm = () => {
    const { username, email, password, passwordConfirmation } = this.state;

    const isInvalid = !username || !email || !password || password !== passwordConfirmation;

    // if invalid input return true
    return isInvalid;

  }

  onSubmitHandler = async (event, signupUser) => {
    //to prevent page reload
    event.preventDefault();

    // retrieving user datas
    const { data } = await signupUser();

    // save user token datas in localStorage
    localStorage.setItem('token', data.signupUser.token);

    // refresh mutation to get fresh datas
    await this.props.refetch();

    //cleanig input when form submitted
    this.clearState();

    //console.log(data);

    //redirect to home page
    this.props.history.push('/');

  }


  render() {
    const { username, email, password } = this.state
    return (

      <div className="App">
        <h2 className="App">Signup</h2>
          <Mutation mutation={SIGNUP_USER} variables={{data: {
            username,
            email,
            password
          }}}>
            {(signupUser, { loading, error }) => {
              if(loading) return <div>loading</div>
              return (
                <div>
                  <form
                    className="form"
                    onSubmit={event => this.onSubmitHandler(event, signupUser)}
                  >
                    <input
                      type="text"
                      name="username"
                      onChange={this.onChangeHandler}
                      value={this.state.username}
                      placeholder="Username"
                    />
                    <input
                      type="text"
                      name="email"
                      onChange={this.onChangeHandler}
                      value={this.state.email}
                      placeholder="Email Address"
                    />
                    <input
                      type="password"
                      name="password"
                      onChange={this.onChangeHandler}
                      value={this.state.password}
                      placeholder="Password"
                    />
                    <input
                      type="password"
                      name="passwordConfirmation"
                      onChange={this.onChangeHandler}
                      value={this.state.passwordConfirmation}
                      placeholder="Confirm Password"
                    />
                    <button className="button-primary" type="submit" disabled={loading || this.validateForm()}>
                      Submit
                    </button>
                  </form>
                  {error && <Error error={error}/>}
                </div>
              );
            }}
          </Mutation>
      </div>
    )
  }
}


export default withRouter(Signup);
