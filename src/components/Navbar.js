import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import Signout from './Auth/Signout';

const Navbar = ({ session }) => (
  <nav>
    {session && session.getCurrentUser ? <AuthNavbar session={session}/> : <UnAuthNavbar/>}
  </nav>
);

const UnAuthNavbar = ( props ) => (
  <ul>
    <li>
      <NavLink exact to="/">Home</NavLink>
    </li>
    <li>
      <NavLink exact to="/search">Search</NavLink>
    </li>
    <li>
      <NavLink exact to="/signin">Signin</NavLink>
    </li>
    <li>
      <NavLink exact to="/signup">Signup</NavLink>
    </li>
  </ul>
);

const AuthNavbar = ({ session }) => (
  <Fragment>
    <ul>
      <li>
        <NavLink exact to="/">Home</NavLink>
      </li>
      <li>
        <NavLink exact to="/search">Search</NavLink>
      </li>
      <li>
        <NavLink exact to="/recipe/add">Add Recipe</NavLink>
      </li>
      <li>
        <NavLink exact to="/profile">Profile</NavLink>
      </li>
      <li>
        <Signout/>
      </li>
    </ul>
    <h2>Welcome, {session.getCurrentUser.username}</h2>
  </Fragment>
);


export default Navbar;
