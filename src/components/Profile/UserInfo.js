import React from 'react';
import { Link } from 'react-router-dom';
import formatDate from '../../utils/format-date';

const UserInfo = ({ session }) => (
  <div className="App">
    <h3>User Info</h3>
    <p>Username: {session.getCurrentUser.username}</p>
    <p>Email: {session.getCurrentUser.email}</p>
    <p>Join Date: {formatDate(session.getCurrentUser.createdAt)}</p>
    <ul>
      <h3>{session.getCurrentUser.username}'s favorites: </h3>
        {session.getCurrentUser.favorites.map(favorite => {
          return <li key={favorite._id}>
                  <Link to={`/recipes/${favorite._id}`}><p>{favorite.name}</p></Link>
                 </li>
        })}
      {session.getCurrentUser.favorites.length < 0 && (
        <p><strong>You have no favorite currently. Go add some!</strong></p>
      )}
    </ul>
  </div>
)


export default UserInfo;
