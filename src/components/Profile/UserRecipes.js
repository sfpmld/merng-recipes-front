import React from 'react';
import { Link } from 'react-router-dom';
import { Query, Mutation } from 'react-apollo';
import { GET_USER_RECIPES, GET_ALL_RECIPES, GET_CURRENT_USER } from '../../graphql/queries/index';
import { DELETE_USER_RECIPE } from '../../graphql/mutations/index';

const deleteUserRecipeHandler = async (deleteUserRecipe) => {
  const confirmDelete = window.confirm('Are you sure you want to delete this recipe?');

  if(confirmDelete){
    await deleteUserRecipe();
    //console.log(data);
  }
}


const UserRecipes = ({ session }) => (
  <Query query={GET_USER_RECIPES} variables={{ username: session.getCurrentUser.username }}>
    {({ data, loading, error }) => {
      if(loading) return <div>loading</div>
      console.log(error);
      if(error) return <div>Error</div>
      return (
        <div>
          <h3>Your Recipes</h3>
          {!data.getUserRecipes.length && <p><strong>You don't own any recipe!</strong></p>}
          <ul>
            {data.getUserRecipes.map(recipe => {
              return (
                <li key={recipe._id}>
                  <p><Link to={`/recipes/${recipe._id}`}>{recipe.name}</Link></p>
                  <p style={{marginBottom: "0"}}>{recipe.likes}</p>

                  <Mutation
                      mutation={DELETE_USER_RECIPE}
                      variables={{_id: recipe._id}}
                      refetch={() => [
                        {query: GET_ALL_RECIPES},
                        {query: GET_CURRENT_USER}
                      ]}
                      update={(cache, data) => {

                        const username = session.getCurrentUser.username;
                        // read cache request for GET_USER_RECIPES, variables: recipe._id
                        const { getUserRecipes } = cache.readQuery({
                          query: GET_USER_RECIPES,
                          variables: { username }
                        });
                        // data to make update cache possible
                        const { deleteUserRecipe } = data.data;

                        // // filtering recipes to purge from deleted recipe
                        const updatedUserRecipe = getUserRecipes.filter(recipe => {
                          return recipe._id !== deleteUserRecipe._id;
                        })

                        // // updating cache manually with fresh datas
                        cache.writeQuery({
                          query: GET_USER_RECIPES,
                          variables: { username },
                          data: {
                            getUserRecipes: [...updatedUserRecipe]
                          }
                        });
                      }}
                      >
                    {(deleteUserRecipe) => {
                      if(loading) return <div>loading</div>
                      if(error) return <div>Error</div>

                      return (
                        <p
                          className="delete-button"
                          onClick={() =>
                            deleteUserRecipeHandler(deleteUserRecipe)
                          }
                        >
                          X
                        </p>
                        );
                    }}
                  </Mutation>

                </li>
              )
            })}
          </ul>
        </div>
      )
    }}
  </Query>
)


export default UserRecipes;
