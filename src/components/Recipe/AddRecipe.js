import React, { useState, useEffect } from 'react';
import CKEditor from 'react-ckeditor-component';
import { Mutation } from 'react-apollo';
import { ADD_RECIPE } from '../../graphql/mutations/index';
import { GET_ALL_RECIPES, GET_USER_RECIPES } from '../../graphql/queries/index';
import withAuth from '../withAuth';

const initialState = {
    name: "",
    category: "Breakfast",
    description: "",
    imageUrl: "",
    instructions: "",
    username: ""
}

const AddRecipe = ( props ) => {

  const [ recipeState, setRecipeState ] = useState(initialState);

  //init state with username of currentUser
  const currentUser = typeof props.session.getCurrentUser !== undefined ? props.session.getCurrentUser.username : '' ;
  useEffect(() => {
    setRecipeState(prevRecipeState => {
      return {
        ...prevRecipeState,
        username: currentUser
      }
    });
  }, [currentUser]);

  const onChangeHandler = event => {
    setRecipeState({
      ...recipeState,
      [event.target.name]: event.target.value
    });
  }

 const handleEditorChange = (event) => {
   const newContent = event.editor.getData()
    setRecipeState({
     ...recipeState,
     instructions: newContent
   })
 }

  const isInvalidForm = () => {
    const { name, category, description, imageUrl, instructions, username } = recipeState;
    const isInvalid = !name || !category || !description || !imageUrl || !instructions || !username;

    return isInvalid;
  }

  const clearInput = () => {
    setRecipeState(initialState);
  }

  const updateCache = (cache, data) => {
    // read cache request for GET_ALL_RECIPES
    const { getAllRecipes } = cache.readQuery({query: GET_ALL_RECIPES})
    // data to insert on top
    const { addRecipe } = data.data

    // updating cache manually with fresh datas
    cache.writeQuery({
      query: GET_ALL_RECIPES,
      data: {
        getAllRecipes: [addRecipe, ...getAllRecipes]
      }
    })
  }

  const onSubmitHandler = async (event, addRecipe) => {
    event.preventDefault();
    // saving recipe
    const data = await addRecipe()
    //console.log(data);
    if(data) props.history.push('/');
  }

  return (
    <Mutation
      mutation={ADD_RECIPE}
      variables={{ data: recipeState }}
      update={updateCache}
      refetchQueries={() => [
        {
          query: GET_USER_RECIPES,
          variables: { username: recipeState.username }
        }
      ]}
      onCompleted={clearInput}
    >
      {(addRecipe, { data, loading, error, refetch }) => {
        if (loading) return <div>loading</div>;
        console.log(error);
        if (error) return <div>Error</div>;
        return (
          <div className="App">
            <h2 className="App">Add Recipe</h2>
            <form
              className="form"
              onSubmit={event => onSubmitHandler(event, addRecipe)}
            >
              <select
                name="category"
                value={recipeState.category}
                onChange={onChangeHandler}
              >
                <option value="Breakfast">Breakfast</option>
                <option value="Lunch">Lunch</option>
                <option value="Dinner">Dinner</option>
                <option value="Snack">Snack</option>
                <option value="Dessert">Dessert</option>
              </select>
              <input
                type="text"
                name="name"
                placeholder="Add name"
                value={recipeState.name}
                onChange={onChangeHandler}
              />
              <input
                type="text"
                name="description"
                placeholder="Add description"
                value={recipeState.description}
                onChange={onChangeHandler}
              />
              <input
                type="text"
                name="imageUrl"
                placeholder="Place your image Url here"
                value={recipeState.imageUrl}
                onChange={onChangeHandler}
              />
              <label htmlFor="instructions">Add instructions</label>
              <CKEditor
                name= "instructions"
                content={recipeState.instructions}
                events={{
                  "change": handleEditorChange
                }}
              />
              {
              // <textarea
              //   name="instructions"
              //   placeholder="Add instructions"
              //   value={recipeState.instructions}
              //   onChange={onChangeHandler}
              // />
              }
              <button
                type="submit"
                className="button-primary"
                disabled={loading || isInvalidForm()}
              >
                Submit
              </button>
            </form>
          </div>
        );
      }}
    </Mutation>
  );

}

export default withAuth(session => session && session.getCurrentUser)(AddRecipe);
