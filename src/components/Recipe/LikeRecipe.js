import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import withSession from '../withSession';
import { LIKE_RECIPE, UNLIKE_RECIPE } from '../../graphql/mutations/index'
import { GET_RECIPE } from '../../graphql/queries/index'



class LikeRecipe extends Component {

  state = {
    username: '',
    liked: false
  }

  // button liked is clicked -> increment like count
  handleClick = likeRecipe => {
    this.setState((prevState, props) => ({
      liked: !prevState.liked
      // toggle like / unlike
    }), this.handleLike(likeRecipe))
  }

  // toggle like / unlike
  handleLike = (likeMutation) => {
      likeMutation().then(async result => {
        console.log(result);
        // refetch() to load fresh datas
        await this.props.refetch()
      });
  }

  updateLikeCache = (cache, { data: { likeRecipe } }) => {

      // read cache request for GET_RECIPE
    const { getRecipe } = cache.readQuery({query: GET_RECIPE, variables: {
      _id: this.props._id
      }});

      // updating cache manually with fresh datas
      const likeCount = this.state.liked ? likeRecipe.like - 1 : likeRecipe.like + 1
      cache.writeQuery({
        query: GET_RECIPE,
        variables: { _id: this.props._id },
        data: {
          getRecipe: { ...getRecipe, likes: likeCount }
        }
      });
  }

  componentDidMount(){
    if(this.props.session.getCurrentUser){
      const { username, favorites } = this.props.session.getCurrentUser
      const { _id } = this.props;

      const alreadyLiked = favorites.findIndex(favorite => {
        return favorite._id === _id
      });

      this.setState({
        username,
        liked: alreadyLiked > -1
      });
    }
  }

  render() {

    const { username } = this.state;

    // if not already liked -> like handler mutation
    if(!this.state.liked) return (
      <Mutation
        mutation={LIKE_RECIPE}
        variables={{
          data: {
            _id: this.props._id,
            username
          }
        }}
        update={this.updateLikeCache}
      >
        {likeRecipe => {
          return (
            // display like button only if user loggedIn
            username && (
              <button className="like-button" onClick={() => this.handleClick(likeRecipe)}>
                {this.state.liked ? "Liked" : "Like"}
              </button>
            )
          );
        }}
      </Mutation>
    );

    // if alredy liked -> unlike handler mutation
    if(this.state.liked) return (
      <Mutation
        mutation={UNLIKE_RECIPE}
        variables={{
          data: {
            _id: this.props._id,
            username
          }
        }}
        update={this.updateUnLikeCache}
      >
        {unLikeRecipe => {
          return (
            // display like button only if user loggedIn
            username && (
              <button className="like-button" onClick={() => this.handleClick(unLikeRecipe)}>
                {this.state.liked ? "Liked" : "Like"}
              </button>
            )
          );
        }}
      </Mutation>
    )
  }
}


export default withSession(LikeRecipe);
