import React from 'react';
import posed from 'react-pose';
import { Link } from 'react-router-dom';

// react pose component configuration
const RecipeItem = posed.li({
  shown: { opacity: 1 },
  hidden: { opacity: 0 }
});

export default ( props ) => {

  return (
    <RecipeItem className="card" style={{ background: `url(${props.imageUrl}) center center / cover no-repeat` }} key={props._id}>
      <span className={props.category}>{props.category}</span>
      <div className="card-text">
        <Link to={`/recipes/${props._id}`}><h4>{props.name}</h4></Link>
      </div>

    </RecipeItem>
  )

}
