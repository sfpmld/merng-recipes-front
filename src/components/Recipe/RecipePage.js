import React from 'react';
import { Query } from 'react-apollo';
import { GET_RECIPE } from '../../graphql/queries/index';

import LikeRecipe from './LikeRecipe';

const RecipePage = (props) => {

  //retrieving _id from url
  const { _id } = props.match.params;
  console.log(_id);

  return (
    <Query query={GET_RECIPE} variables={{_id}}>
      {({ data, loading, error }) => {
        console.log(data);
        if(loading) return <div>loading</div>
        console.log(error);
        if(error) return <div>Error</div>
        return (
          <div className="App">
            <div style={{ background: `url(${data.getRecipe.imageUrl}) center center / cover no-repeat` }} className="recipe-image"/>
              <div className="recipe">
                <div className="recipe-header">
                  <h2 className="recipe-name">
                    <strong>{data.getRecipe.name}</strong>
                  </h2>
                  <h5><strong>{data.getRecipe.category}</strong></h5>
                    <p>Created by <strong>{data.getRecipe.username}</strong></p>
                    <p>{data.getRecipe.likes} <span role="img" aria-label="heart">❤️</span></p>
                </div>
                  <blockquote className="recipe-description">{data.getRecipe.description}</blockquote>
                  <h3 className="recipe-instructions__title">Instructions</h3>
                  <div
                    className="recipe-instructions"
                    dangerouslySetInnerHTML={{
                      __html: data.getRecipe.instructions
                    }}
                  />
                <LikeRecipe _id={_id}/>
              </div>
          </div>
        )
      }}
    </Query>
  );


}


export default RecipePage;
