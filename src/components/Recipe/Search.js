import React, { useState } from 'react';
import { ApolloConsumer } from 'react-apollo';
import SearchItem from './SearchItem';
import { SEARCH_RECIPES } from '../../graphql/queries/index';

const Search = ( props ) => {

  // Initialize state;
  const [ searchState, setSearchState ] = useState([]);

  const onChangeHandler = (data) => {
    //console.log(data);
    setSearchState(data.searchRecipes);
    console.log(searchState)
  }

  return <ApolloConsumer>
    {(client) => {
      return (
        <div className="App">
          <input
            type="search"
            name="search"
            placeholder="Search for Recipes"
            onChange={ async (event) => {
              event.persist();
              const { data } = await client.query({
                query: SEARCH_RECIPES,
                variables: {
                  data: event.target.value
                }
              });

              onChangeHandler(data);
            }}/>
          <SearchItem searchedResults={searchState}/>
        </div>
      );
    }}
  </ApolloConsumer>
};


export default Search;
