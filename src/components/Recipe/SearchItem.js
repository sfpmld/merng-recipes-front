import React from 'react';
import { Link } from 'react-router-dom';

const SearchItem = ( { searchedResults } ) => (

  <ul>
    {searchedResults.map(recipe => {
      return (
        <li key={recipe._id}>
          <Link to={`/recipes/${recipe._id}`}>
            <h4>{recipe.name}</h4>
          </Link>
          <p>Likes: {recipe.likes}</p>
        </li>
      );
    })}
  </ul>
)


export default SearchItem ;
