import React from 'react';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom'
import { GET_CURRENT_USER } from '../graphql/queries/index';


const withAuth = conditionFunc => Component => props => (

  <Query query={GET_CURRENT_USER}>
    {({ data, loading, error }) => {
      if(loading) return <div>loading</div>
      console.log(error);
      if(error) return <div>Error</div>

      // Conditional check : if false redirecting to home page
      return conditionFunc(data) ? <Component {...props} /> : <Redirect to="/" />
    }}
  </Query>

)

export default withAuth;
