import React from 'react';
import { Query } from 'react-apollo';
import { GET_CURRENT_USER } from '../graphql/queries/index';

const withSession = Component => ( props ) => (

  <Query query={GET_CURRENT_USER}>
    {({ data, loading, error, refetch }) => {
      if(loading) return null
      if(error) console.log(error)

      //console.log(data);

      return (
        <Component {...props} refetch={refetch} session={data}/>
      )
    }}
  </Query>

)


export default withSession;
