import { gql } from 'apollo-boost';


export const recipeFragments = {
  recipe: gql`
    fragment CompleteRecipe on	Recipe {
      _id
      name
      description
      category
      imageUrl
      instructions
      likes
      username
      createdAt
    }
  `,
  addRecipe: gql`
    fragment AddRecipe on	Recipe {
      name
      category
      description
      imageUrl
      likes
      instructions
      username
      createdAt
    }
  `,
  like: gql`
    fragment LikeRecipe on Recipe {
      _id
      name
      likes
    }
  `
}
