import { gql } from 'apollo-boost';
import { recipeFragments } from '../fragments/fragments';

export const SIGNIN_USER = gql`
mutation($data: SigninInput!) {
  signinUser(data: $data){
    token
  }
}
`;

export const SIGNUP_USER = gql`
mutation($data: SignupInput!) {
  signupUser(data: $data){
    token
  }
}
`;

export const ADD_RECIPE = gql`
mutation($data: AddRecipeInput!) {
  addRecipe(data: $data){
    ...AddRecipe
  }
}
${recipeFragments.addRecipe}
`;

export const DELETE_USER_RECIPE = gql`
mutation($_id: ID!) {
  deleteUserRecipe(_id: $_id){
    _id
    name
  }
}
`;

export const LIKE_RECIPE = gql`
mutation($data: LikeInput!) {
  likeRecipe(data: $data){
    ...LikeRecipe
  }
}
${recipeFragments.like}
`;

export const UNLIKE_RECIPE = gql`
mutation($data: LikeInput!) {
  unLikeRecipe(data: $data){
    ...LikeRecipe
  }
}
${recipeFragments.like}
`;
