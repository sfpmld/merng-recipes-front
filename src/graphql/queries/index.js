import { gql } from 'apollo-boost';
import { recipeFragments } from '../fragments/fragments';


export const GET_ALL_RECIPES = gql`
query {
  getAllRecipes {
    _id
    name
    category
    imageUrl
  }
}
`
export const GET_RECIPE = gql`
  query($_id: ID!) {
    getRecipe(_id: $_id) {
      ...CompleteRecipe
    }
  }
  ${recipeFragments.recipe}
`
export const SEARCH_RECIPES = gql`
  query($data: String) {
    searchRecipes(data: $data){
      ...CompleteRecipe
    }
  }
  ${recipeFragments.recipe}
`;

export const GET_CURRENT_USER = gql`
query {
  getCurrentUser {
    username
    createdAt
    email
    favorites {
      _id
      name
      likes
    }
  }
}
`;

export const GET_USER_RECIPES = gql`
  query($username: String!) {
    getUserRecipes(username: $username){
      ...CompleteRecipe
    }
  }
  ${recipeFragments.recipe}
`;
