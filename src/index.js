import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import Navbar from './components/Navbar';
import Search from './components/Recipe/Search';
import AddRecipe from './components/Recipe/AddRecipe';
import RecipePage from './components/Recipe/RecipePage';
import Profile from './components/Profile/Profile';
import Signin from './components/Auth/Signin';
import Signup from './components/Auth/Signup';
import withSession from './components/withSession';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// creating Apollo Client
const GRAPHQL_ENDPOINT = 'http://localhost:3000/graphql';


const client = new ApolloClient({
  uri: GRAPHQL_ENDPOINT,
  fetchOptions: {
    credentials: 'include'
  },
  request: operation => {
    const token = localStorage.getItem('token');
    operation.setContext({
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  onError: ({ networkError }) => {
    if(networkError){
      console.log('Network Error', networkError);

      if(networkError.statusCode === 401){
        localStorage.setItem('token','');
      }
    }
  }
});


const Root = ({ refetch, session }) => (
  <BrowserRouter>
    <Fragment>
      <Navbar session={session}/>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/search" component={Search} />
        <Route exact path="/recipe/add" render={(props) => {
          return <AddRecipe history={props.history} refetch={refetch} session={session} />
        }} />
        <Route exact path='/recipes/:_id' component={RecipePage} />
        <Route exact path="/profile" render={() => {
          return <Profile session={session}/>
        }} />
        <Route
          exact
          path="/signin"
          render={() => {
            return <Signin refetch={refetch} />;
          }}
        />
        <Route
          path="/signup"
          render={() => {
            return <Signup refetch={refetch} />;
          }}
        />
        <Redirect to="/" />
      </Switch>
    </Fragment>
  </BrowserRouter>
);


const RootWithSession = withSession(Root);


ReactDOM.render(
  <ApolloProvider client={client}>
    <RootWithSession />
  </ApolloProvider>
  , document.getElementById('root'));



