const formatDate = (date) => {

  const day = new Date(date).toLocaleDateString('FR');
  const time = new Date(date).toLocaleTimeString('FR');

  return `${day} at ${time}`;
}

export default formatDate;
